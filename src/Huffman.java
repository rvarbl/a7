
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {
    /**
     * Inner class for Huffman tree nodes.
     */
    static class Node {
        /**
         * @param freq          - how many times original value appears in given bytearray.
         * @param originalValue - original value in bytearray.
         * @param left          - reference to next node if given node is not an leaf node.
         * @param right         - reference to next node if given node is not an leaf node.
         */
        Node(int freq, Byte originalValue, Node left, Node right) {
            this.frequency = freq;
            this.originalValue = originalValue;
            this.left = left;
            this.right = right;
        }

        /**
         * @param originalValue - original value in bytearray.
         * @param encodedValue  - value after using Huffman coding.
         * @param frequency     - how many times original value appears in given bytearray.
         */
        Node(Byte originalValue, String encodedValue, int frequency) {
            this.originalValue = originalValue;
            this.encodedValue = encodedValue;
            this.frequency = frequency;
        }

        int frequency;
        Byte originalValue;
        String encodedValue;
        Node left;
        Node right;

        public boolean isLeaf() {
            return left == null && right == null;
        }
    }

    List<Node> huffmanList = new ArrayList<>();

    /**
     * Constructor to build the Huffman code for a given bytearray.
     *
     * @param original source data
     */
    Huffman(byte[] original) {
        if (original.length < 1) {
            throw new RuntimeException("Empty array given!");
        }
        createHuffmanByteMap(original);
    }

    /**
     * Creates dictionary showing how often certain byte appears in array.
     * Creates Huffman tree from frequency map.
     * Gets encoded values for Huffman tree leaf nodes and populates huffmanList with said leaf nodes.
     *
     * @param original source data
     */
    private void createHuffmanByteMap(byte[] original) {
        Map<Byte, Integer> frequency = countFrequency(original);
        Node huffmanNode = createHuffmanTree(frequency);
        if (huffmanNode.isLeaf()) {
            huffmanList.add(huffmanNode);
        } else {
            createHuffmanTreeCodes(huffmanNode, "");
        }
    }

    /**
     * Create Huffman tree from given frequency map.
     *
     * @param byteMap frequency map.
     * @return root node.
     */
    private Node createHuffmanTree(Map<Byte, Integer> byteMap) {
        Queue<Node> nodes = new ArrayDeque<>();
        while (byteMap.size() > 0) {
            Map.Entry<Byte, Integer> minEntry = popMinValue(byteMap);
            if (minEntry != null) {
                nodes.add(new Node(minEntry.getValue(), minEntry.getKey(), null, null));
            }
        }
        if (nodes.size() == 1) {
            Node node = nodes.remove();
            node.encodedValue = "0";
            return node;
        }
        while (nodes.size() > 1) {
            Node left = nodes.remove();
            Node right = nodes.remove();
            int nodeFreq = getFrequency(left) + getFrequency(right);
            nodes.add(new Node(nodeFreq, null, left, right));
        }
        return nodes.remove();
    }

    /**
     * Gives each leaf node value a new code and Populates huffmanList with leaf nodes.
     *
     * @param node root node.
     * @param code Huffman code: 0 - if traveling by left node , 1 - if traveling by right node.
     */
    private void createHuffmanTreeCodes(Node node, String code) {
        if (node == null) return;
        if (node.isLeaf()) {
            huffmanList.add(new Node(node.originalValue, code, node.frequency));
        } else {
            createHuffmanTreeCodes(node.left, code + "0");
            createHuffmanTreeCodes(node.right, code + "1");
        }
    }


    /**
     * Helper method to get smallest value (frequency) from dictionary.
     *
     * @param byteMap frequency map.
     * @return entry with the first smallest value.
     */
    private Map.Entry<Byte, Integer> popMinValue(Map<Byte, Integer> byteMap) {
        Integer min = Collections.min(byteMap.values());
        for (Map.Entry<Byte, Integer> entry : byteMap.entrySet()) {
            if (entry.getValue().equals(min)) {
                byteMap.remove(entry.getKey());
                return entry;
            }
        }
        return null;
    }


    /**
     * Creates map of how many times certain byte appears in byte array.
     *
     * @param original input array to be parsed.
     * @return map of bytes and number of appearances in the array.
     */
    private Map<Byte, Integer> countFrequency(byte[] original) {
        Map<Byte, Integer> byteMap = new HashMap<>();
        for (byte b : original) {
            if (byteMap.containsKey(b)) {
                byteMap.put(b, byteMap.get(b) + 1);
            } else {
                byteMap.put(b, 1);
            }
        }
        return byteMap;
    }

    /**
     * Helper method to get nodes frequency parameter.
     *
     * @param node node to be parsed.
     * @return frequency.
     */
    public static int getFrequency(Node node) {
        if (node == null) return 0;
        return node.frequency;
    }


    /**
     * Length of encoded data in bits.
     *
     * @return number of bits
     */
    public int bitLength() {
        int length = 0;
        for (Node node : huffmanList) {
            int frequency = node.frequency;
            int code = node.encodedValue.length();
            length += frequency * code;
        }
        return length;
    }


    /**
     * Encoding the byte array using this prefix code.
     *
     * @param origData original data
     * @return encoded data
     */
    public byte[] encode(byte[] origData) {
        BitSet result = new BitSet();
        int index = 0;
        for (byte b : origData) {
            Node node = findNodeByOriginal(b);
            if (node != null) {
                for (char c : node.encodedValue.toCharArray()) {
                    if (c == '1') {
                        result.set(index);
                    }
                    index++;
                }
            }
        }
        return result.toByteArray();
    }


    /**
     * Decoding the byte array using this prefixcode.
     *
     * @param encodedData encoded data
     * @return decoded data (hopefully identical to original)
     */
    public byte[] decode(byte[] encodedData) {
        List<Byte> result = new ArrayList<>();
        BitSet bitset = BitSet.valueOf(encodedData);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < this.bitLength(); i++) {
            if (bitset.get(i)) {
                builder.append("1");
            } else {
                builder.append("0");
            }
            for (Node g : huffmanList) {
                if (g.encodedValue.equals(builder.toString())) {
                    result.add(g.originalValue);
                    builder = new StringBuilder();
                }
            }
        }


        byte[] r = new byte[result.size()];
        for (int i = 0; i < result.size(); i++) {
            r[i] = result.get(i);
        }
        return r;
    }

    /**
     * Helper method to find node by its original byte value.
     *
     * @param b original byte value to be found.
     * @return node.
     */
    private Node findNodeByOriginal(byte b) {
        for (Node node : huffmanList) {
            if (b == node.originalValue) {
                return node;
            }
        }
        return null;
    }


    /**
     * Main method.
     */
    public static void main(String[] params) {
//        String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
        String tekst = "TERE1";
        byte[] orig = tekst.getBytes();

        Huffman huf = new Huffman(orig);
        byte[] kood = huf.encode(orig);
        byte[] orig2 = huf.decode(kood);
        for (byte x : orig) {
            System.out.println(x);
        }
        System.out.println("---");
        for (byte z : kood) {
            System.out.println(z);
        }
        System.out.println("---");
        for (byte y : orig2) {
            System.out.println(y);
        }
        // must be equal: orig, orig2
        System.out.println(Arrays.equals(orig, orig2));
        int lngth = huf.bitLength();
        System.out.println("Length of encoded data in bits: " + lngth);
    }
}

